#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import urlparse
import urllib
import xml.dom.minidom
try:
    from DAV import propfind
    from DAV.errors import *
except ImportError:
    from pywebdav.lib import propfind
    from pywebdav.lib.errors import *
from trytond.protocols.webdav import TrytonDAVInterface, CACHE
from trytond.pool import Pool
from trytond.transaction import Transaction

_TRYTON_RELOAD = False
domimpl = xml.dom.minidom.getDOMImplementation()

TrytonDAVInterface.PROPS['urn:ietf:params:xml:ns:carddav'] = (
        'address-data',
        'addressbook-data',
        'addressbook-home-set',
    )
TrytonDAVInterface.M_NS['urn:ietf:params:xml:ns:carddav'] = '_get_carddav'

_mk_prop_response = propfind.PROPFIND.mk_prop_response

def mk_prop_response(self, uri, good_props, bad_props, doc):
    res = _mk_prop_response(self, uri, good_props, bad_props, doc)
    dbname, uri = TrytonDAVInterface.get_dburi(uri)
    if uri in ('Contacts', 'Contacts/'):
        ad = doc.createElement('addressbook')
        ad.setAttribute('xmlns', 'urn:ietf:params:xml:ns:carddav')
        vc = doc.createElement('vcard-collection')
        vc.setAttribute('xmlns', 'http://groupdav.org/')
        cols = res.getElementsByTagName('D:collection')
        if cols:
            cols[0].parentNode.appendChild(ad)
            cols[0].parentNode.appendChild(vc)
    return res

propfind.PROPFIND.mk_prop_response = mk_prop_response

def _get_carddav_address_data(self, uri):
    dbname, dburi = self._get_dburi(uri)
    if not dbname:
        raise DAV_NotFound
    pool = Pool(Transaction().cursor.database_name)
    try:
        collection_obj = pool.get('webdav.collection')
    except KeyError:
        raise DAV_NotFound
    try:
        res = collection_obj.get_address_data(dburi, cache=CACHE)
    except (DAV_Error, DAV_NotFound, DAV_Secret, DAV_Forbidden):
        raise
    except Exception:
        raise DAV_Error(500)
    return res

TrytonDAVInterface._get_carddav_address_data = _get_carddav_address_data
TrytonDAVInterface._get_carddav_addressbook_data = _get_carddav_address_data

# Needed to make it work with iOS 5.0
def _get_carddav_addressbook_home_set(self, uri):
    dbname, dburi = self._get_dburi(uri)
    if not dbname:
        raise DAV_NotFound
    pool = Pool(Transaction().cursor.database_name)
    try:
        collection_obj = pool.get('webdav.collection')
    except KeyError:
        raise DAV_NotFound
    try:
        res = collection_obj.get_addressbook_home_set(dburi, cache=CACHE)
    except AttributeError:
        raise DAV_NotFound
    except (DAV_Error, DAV_NotFound, DAV_Secret, DAV_Forbidden), exception:
        self._log_exception(exception)
        raise
    except Exception, exception:
        self._log_exception(exception)
        raise DAV_Error(500)
    uparts = list(urlparse.urlsplit(uri))
    uparts[2] = urllib.quote(dbname + res)
    doc = domimpl.createDocument(None, 'href', None)
    href = doc.documentElement
    href.tagName = 'D:href'
    #iPhone doesn't handle "http" in href
    #huri = doc.createTextNode(urlparse.urlunsplit(uparts))
    huri = doc.createTextNode(urllib.quote('/' + dbname + res))
    href.appendChild(huri)
    return href

TrytonDAVInterface._get_carddav_addressbook_home_set = _get_carddav_addressbook_home_set

