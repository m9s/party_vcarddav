#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import uuid
from trytond.model import ModelSQL, ModelView, fields
from trytond.report import Report
from trytond.backend import TableHandler, FIELDS
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.version import VERSION


class Party(ModelSQL, ModelView):
    _name = 'party.party'
    uuid = fields.Char('UUID', required=True,
            help='Universally Unique Identifier')
    vcard = fields.Binary('VCard')

    def __init__(self):
        super(Party, self).__init__()
        self._sql_constraints += [
                ('uuid_uniq', 'UNIQUE(uuid)',
                    'The UUID of the party must be unique!'),
        ]

    def init(self, module_name):
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        if not table.column_exist('uuid'):
            table.add_raw_column('uuid',
                    FIELDS[self.uuid._type].sql_type(self.uuid),
                    FIELDS[self.uuid._type].sql_format, None, None)
            cursor.execute('SELECT id FROM "' + self._table + '"')
            for id, in cursor.fetchall():
                cursor.execute('UPDATE "' + self._table + '" ' \
                        'SET "uuid" = %s WHERE id = %s',
                        (self.default_uuid(), id))
        super(Party, self).init(module_name)

    def default_uuid(self):
        return str(uuid.uuid4())

    def create(self, vals):
        collection_obj = Pool().get('webdav.collection')

        res = super(Party, self).create(vals)
        # Restart the cache for vcard
        collection_obj.vcard.reset()
        return res

    def copy(self, ids, default=None):
        int_id = isinstance(ids, (int, long))
        if int_id:
            ids = [ids]

        if default is None:
            default = {}

        new_ids = []
        default['vcard'] = False
        new_ids = []
        for party_id in ids:
            current_default = default.copy()
            current_default['uuid'] = self.default_uuid()
            new_id = super(Party, self).copy(party_id, default=current_default)
            new_ids.append(new_id)

        if int_id:
            return new_ids[0]
        return new_ids

    def write(self, ids, vals):
        collection_obj = Pool().get('webdav.collection')

        res = super(Party, self).write(ids, vals)
        # Restart the cache for vcard
        collection_obj.vcard.reset()
        return res

    def delete(self, ids):
        collection_obj = Pool().get('webdav.collection')

        res = super(Party, self).delete(ids)
        # Restart the cache for vcard
        collection_obj.vcard.reset()
        return res

    def _vcard_n2values(self, party_id, vcard):
        return {'name': vcard.fn.value}

    def vcard2values(self, party_id, vcard):
        '''
        Convert vcard to values for create or write

        :param party_id: the party id for write or None for create
        :param vcard: a vcard instance of vobject
        :return: a dictionary with values
        '''
        import vobject
        pool = Pool()
        address_obj = pool.get('party.address')
        category_obj = pool.get('party.category')
        contact_mechanism_obj = pool.get('party.contact_mechanism')

        res = self._vcard_n2values(party_id, vcard)
        if not hasattr(vcard, 'n'):
            vcard.add('n')
            vcard.n.value = vobject.vcard.Name()
        if hasattr(vcard, 'rev'):
            # Incompatibility between iOS 5.0 and Sogo Connector
            # Sogo Connector uses format of vCard 4.0: REV:19951031T222710Z
            # iOS 5.0 uses format of vCard 3.0: REV:1995-10-31T22:27:10Z
            # SoGo Connector does unallowed escaping on 3.0 format:
            # REV:1995-10-31T22:27:10Z -> REV:1995-10-31T22\:27\:10Z
            # This leads to a serialization error in vobject
            # So we convert 3.0 format to 4.0 format.
            # This is tested with common clients
            vcard.rev.value = vcard.rev.value.replace(
                '-', '').replace(':', '')
        res['vcard'] = vcard.serialize()
        if not party_id:
            if hasattr(vcard, 'uid'):
                res['uuid'] = vcard.uid.value
            if hasattr(vcard, 'categories'):
                categories = set()
                for category in vcard.contents.get('categories', []):
                    category_ids = category_obj.vcard2values(category)
                    categories = categories.union(category_ids)
                res['categories'] = [('set', list(categories))]
            res['addresses'] = []
            for adr in vcard.contents.get('adr', []):
                vals = address_obj.vcard2values(adr)
                res['addresses'].append(('create', vals))
            res['contact_mechanisms'] = []
            for email in vcard.contents.get('email', []):
                vals = contact_mechanism_obj.vcard2values(email, 'email')
                res['contact_mechanisms'].append(('create', vals))
            for tel in vcard.contents.get('tel', []):
                vals = contact_mechanism_obj.vcard2values(tel, 'phone')
                res['contact_mechanisms'].append(('create', vals))
            for url in vcard.contents.get('url', []):
                vals = contact_mechanism_obj.vcard2values(url, 'website')
                res['contact_mechanisms'].append(('create', vals))
        else:
            party = self.browse(party_id)
            i = 0
            categories = set()
            for category in party.categories:
                try:
                    category = vcard.contents.get('categories', [])[i]
                except IndexError:
                    i += 1
                    continue
                if not hasattr(category, 'value'):
                    i += 1
                    continue
                category_ids = category_obj.vcard2values(category)
                categories = categories.union(category_ids)
                i += 1
            try:
                new_categories = vcard.contents.get('categories', [])[i:]
            except IndexError:
                new_categories = []
            for category in new_categories:
                if not hasattr(category, 'value'):
                    continue
                category_ids = category_obj.vcard2values(category)
                categories = categories.union(category_ids)
            res['categories'] = [('set', list(categories))]
            i = 0
            res['addresses'] = []
            addresses_todelete = []
            for address in party.addresses:
                try:
                    adr = vcard.contents.get('adr', [])[i]
                except IndexError:
                    addresses_todelete.append(address.id)
                    i += 1
                    continue
                if not hasattr(adr, 'value'):
                    addresses_todelete.append(address.id)
                    i += 1
                    continue
                vals = address_obj.vcard2values(adr)
                res['addresses'].append(('write', address.id, vals))
                i += 1
            if addresses_todelete:
                res['addresses'].append(('delete', addresses_todelete))
            try:
                new_addresses = vcard.contents.get('adr', [])[i:]
            except IndexError:
                new_addresses = []
            for adr in new_addresses:
                if not hasattr(adr, 'value'):
                    continue
                vals = address_obj.vcard2values(adr)
                res['addresses'].append(('create', vals))

            i = 0
            res['contact_mechanisms'] = []
            contact_mechanisms_todelete = []
            for cm in party.contact_mechanisms:
                if cm.type != 'email':
                    continue
                try:
                    email = vcard.contents.get('email', [])[i]
                except IndexError:
                    contact_mechanisms_todelete.append(cm.id)
                    i += 1
                    continue
                vals = contact_mechanism_obj.vcard2values(email, 'email')
                res['contact_mechanisms'].append(('write', cm.id, vals))
                i += 1
            try:
                new_emails = vcard.contents.get('email', [])[i:]
            except IndexError:
                new_emails = []
            for email in new_emails:
                if not hasattr(email, 'value'):
                    continue
                vals = contact_mechanism_obj.vcard2values(email, 'email')
                res['contact_mechanisms'].append(('create', vals))

            i = 0
            for cm in party.contact_mechanisms:
                if cm.type not in ('phone', 'mobile', 'fax'):
                    continue
                try:
                    tel = vcard.contents.get('tel', [])[i]
                except IndexError:
                    contact_mechanisms_todelete.append(cm.id)
                    i += 1
                    continue
                vals = contact_mechanism_obj.vcard2values(tel, 'phone')
                res['contact_mechanisms'].append(('write', cm.id, vals))
                i += 1
            try:
                new_tels = vcard.contents.get('tel', [])[i:]
            except IndexError:
                new_tels = []
            for tel in new_tels:
                if not hasattr(tel, 'value'):
                    continue
                vals = contact_mechanism_obj.vcard2values(tel, 'phone')
                res['contact_mechanisms'].append(('create', vals))

            i = 0
            for cm in party.contact_mechanisms:
                if cm.type != 'website':
                    continue
                try:
                    url = vcard.contents.get('url', [])[i]
                except IndexError:
                    contact_mechanisms_todelete.append(cm.id)
                    i += 1
                    continue
                vals = contact_mechanism_obj.vcard2values(url, 'website')
                res['contact_mechanisms'].append(('write', cm.id, vals))
                i += 1
            try:
                new_urls = vcard.contents.get('url', [])[i:]
            except IndexError:
                new_urls = []
            for url in new_urls:
                if not hasattr(url, 'value'):
                    continue
                vals = contact_mechanism_obj.vcard2values(url, 'website')
                res['contact_mechanisms'].append(('create', vals))

            if contact_mechanisms_todelete:
                res['contact_mechanisms'].append(('delete',
                    contact_mechanisms_todelete))
        return res

Party()


class Category(ModelSQL, ModelView):
    _name = 'party.category'

    def vcard2values(self, category):
        category_ids = self.search([('name', 'in', category.value)])
        if category_ids:
            return category_ids
        return None

    def category2vcard(self, categories, vcard):
        vcategorylists = vcard.contents.get('categories', [])
        if not categories:
            for vcategorylist in vcategorylists:
                vcard.contents['categories'].remove(vcategorylist)
            return vcard

        tcategories = [category.name for category in categories]
        tcategories_found = []
        for vcategories in vcategorylists:
            if not isinstance(vcategories, (list, tuple)):
                vcategories = [vcategories]
            i = 0
            for category in vcategories:
                if category not in tcategories or category in tcategories_found:
                    del vcategories[i]
                else:
                    tcategories_found.append(category)
                i += 1

        new_categories = []
        for category in categories:
            if category.name not in tcategories_found:
                new_categories.append(category.name)
        if new_categories:
            try:
                categorylist = vcard.contents.get('categories', [])[1]
            except IndexError:
                categorylist = None
            if not categorylist:
                categorylist = vcard.add('categories')
            categorylist.value = new_categories + tcategories_found

        return vcard

Category()


class Address(ModelSQL, ModelView):
    _name = 'party.address'

    def vcard2values(self, adr):
        '''
        Convert adr from vcard to values for create or write

        :param adr: a adr from vcard instance of vobject
        :return: a dictionary with values
        '''
        pool = Pool()
        country_obj = pool.get('country.country')
        subdivision_obj = pool.get('country.subdivision')
        user_obj = pool.get('res.user')

        vals = {}
        vals['street'] = adr.value.street or ''
        vals['city'] = adr.value.city or ''
        vals['zip'] = adr.value.code or ''
        if adr.value.country:
            user = user_obj.browse(Transaction().user)
            with Transaction().set_context(language=user.language.code):
                country_ids = country_obj.search([
                    ('rec_name', '=', adr.value.country),
                    ], limit=1)
                if country_ids:
                    vals['country'] = country_ids[0]
                    if adr.value.region:
                        subdivision_ids = subdivision_obj.search([
                                ('rec_name', '=', adr.value.region),
                                ('country', '=', country_ids[0]),
                                ], limit=1)
                        if subdivision_ids:
                            vals['subdivision'] = subdivision_ids[0]
        return vals

    def address2vcard(self, addresses, vcard):
        user_obj = Pool().get('res.user')
        import vobject
        i = 0
        for address in addresses:
            try:
                adr = vcard.contents.get('adr', [])[i]
            except IndexError:
                adr = None
            if not adr:
                adr = vcard.add('adr')
            if not hasattr(adr, 'value'):
                adr.value = vobject.vcard.Address()
            adr.value.street = address.street and address.street + (
                address.streetbis and  (" " + address.streetbis) or '') or ''
            adr.value.city = address.city or ''
            adr.value.code = address.zip or ''
            adr.type_param = self._address2type_paramlist(address, adr)
            user = user_obj.browse(Transaction().user)
            with Transaction().set_context(language=user.language.code):
                if address.subdivision:
                    adr.value.region = address.subdivision.name or ''
                if address.country:
                    adr.value.country = address.country.name or ''
            i += 1
        try:
            older_addresses = vcard.contents.get('adr', [])[i:]
        except IndexError:
            older_addresses = []
        for adr in older_addresses:
            vcard.contents['adr'].remove(adr)
        return vcard

    def _address2type_paramlist(self, address, vcard_adr):
        res = []
        if hasattr(vcard_adr, 'type_paramlist'):
            res = [x.lower() for x in vcard_adr.type_paramlist]
        return res

Address()


class ContactMechanism(ModelSQL, ModelView):
    _name = 'party.contact_mechanism'

    def vcard2values(self, cm, cm_type):
        vals = {}
        vals['type'] = cm_type
        vals['value'] = cm.value
        if hasattr(cm, 'type_paramlist'):
            if cm_type == 'phone':
                if 'cell' in [x.lower() for x in cm.type_paramlist]:
                    vals['type'] = 'mobile'
                elif 'fax' in [x.lower() for x in cm.type_paramlist]:
                    vals['type'] = 'fax'
        return vals

    def _cm2type_paramlist(self, cm, vcard_cm):
        res = []
        if hasattr(vcard_cm, 'type_paramlist'):
            res = [x.lower() for x in vcard_cm.type_paramlist]
        if cm.type == 'email':
            if 'internet' not in res:
                res.append('internet')
        elif cm.type in ('phone', 'mobile'):
            if cm.type == 'mobile':
                if 'cell' not in res:
                    res.append('cell')
            else:
                if 'voice' not in res:
                    res.append('voice')
        elif cm.type == 'fax':
            if 'fax' not in res:
                res.append('fax')
        return res

    def cm2vcard(self, cms, vcard):
        email_count = 0
        tel_count = 0
        url_count = 0
        for cm in cms:
            if cm.type == 'email':
                try:
                    email = vcard.contents.get('email', [])[email_count]
                except IndexError:
                    email = None
                if not email:
                    email = vcard.add('email')
                email.value = cm.value
                email.type_param = self._cm2type_paramlist(cm, email)
                email_count += 1
            elif cm.type in ('phone', 'mobile', 'fax'):
                try:
                    tel = vcard.contents.get('tel', [])[tel_count]
                except IndexError:
                    tel = None
                if not tel:
                    tel = vcard.add('tel')
                tel.value = cm.value
                tel.type_param = self._cm2type_paramlist(cm, tel)
                tel_count += 1
            elif cm.type == 'website':
                try:
                    url = vcard.contents.get('url', [])[url_count]
                except IndexError:
                    url = None
                if not url:
                    url = vcard.add('url')
                url.value = cm.value
                url.type_param = self._cm2type_paramlist(cm, url)
                url_count += 1

        try:
            older_emails = vcard.contents.get('email', [])[email_count:]
        except IndexError:
            older_emails = []
        for email in older_emails:
            vcard.contents['email'].remove(email)

        try:
            older_tels = vcard.contents.get('tel', [])[tel_count:]
        except IndexError:
            older_tels = []
        for tel in older_tels:
            vcard.contents['tel'].remove(tel)

        try:
            older_urls = vcard.contents.get('url', [])[url_count:]
        except IndexError:
            older_urls = []
        for url in older_urls:
            vcard.contents['url'].remove(url)

        return vcard


ContactMechanism()


class ActionReport(ModelSQL, ModelView):
    _name = 'ir.action.report'

    def __init__(self):
        super(ActionReport, self).__init__()
        new_ext = ('vcf', 'VCard file')
        if new_ext not in self.extension.selection:
            self.extension = copy.copy(self.extension)
            self.extension.selection = copy.copy(self.extension.selection)
            self.extension.selection.append(new_ext)
            self._reset_columns()

ActionReport()


class VCard(Report):
    _name = 'party_vcarddav.party.vcard'

    def execute(self, ids, datas):
        party_obj = Pool().get('party.party')
        action_report_obj = Pool().get('ir.action.report')

        action_report_ids = action_report_obj.search([
            ('report_name', '=', self._name)
            ])
        if not action_report_ids:
            raise Exception('Error', 'Report (%s) not find!' % self._name)
        action_report = action_report_obj.browse(action_report_ids[0])

        parties = party_obj.browse(ids)

        data = ''.join(self.create_vcard(party).serialize() for party in parties)

        return ('vcf', buffer(data), False, action_report.name)

    def _party_name2vcard_n_values(self, party):
        '''
        return: Dictionary with the allowed keys:
            family: family names (list or string)
            given: given names (list or string)
            additional: additional names (list or string)
            prefix: honoric prefixes (list or string)
            suffix: honoric suffixes (list or string)
        '''
        return {'family': party.name}

    def create_vcard(self, party):
        '''
        Return a vcard instance of vobject for the party

        :param party: a BrowseRecord of party.party
        :return: a vcard instance of vobject
        '''
        import vobject
        pool = Pool()
        address_obj = pool.get('party.address')
        category_obj = pool.get('party.category')
        contact_mechanism_obj = pool.get('party.contact_mechanism')

        if party.vcard:
            vcard = vobject.readOne(str(party.vcard))
        else:
            vcard = vobject.vCard()
        if not hasattr(vcard, 'n'):
            vcard.add('n')
        nvals = self._party_name2vcard_n_values(party)
        vcard.n.value = vobject.vcard.Name(**nvals)
        if not hasattr(vcard, 'fn'):
            vcard.add('fn')
        vcard.fn.value = party.full_name
        if not hasattr(vcard, 'uid'):
            vcard.add('uid')
        vcard.uid.value = party.uuid
        if not hasattr(vcard, 'prodid'):
            vcard.add('prodid')
        vcard.prodid.value = '-//tryton.org//trytond ' + VERSION + '//EN'
        vcard = address_obj.address2vcard(party.addresses, vcard)
        vcard = contact_mechanism_obj.cm2vcard(party.contact_mechanisms, vcard)
        vcard = category_obj.category2vcard(party.categories, vcard)
        return vcard

VCard()
